import React from "react";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import dynamic from "next/dynamic";
import "bootstrap/dist/css/bootstrap.min.css";

const Header = dynamic(() => import("./header"), {
  ssr: false,
});
export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <Header></Header>
      <body>{children}</body>
    </html>
  );
}
