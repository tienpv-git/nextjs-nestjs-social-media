"use client";
import { TextField, Container, Grid } from "@mui/material";

const Search = () => {
  return (
    <>
      <Container maxWidth="lg">
        <Grid>
          <Grid
            item
            xs={12}
            container
            sx={{ justifyContent: "center", alignItems: "center", mt: 3 }}
          >
            <TextField id="filled-basic" label="Filled" variant="filled" />
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
export default Search;
