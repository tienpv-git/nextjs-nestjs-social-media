export interface classRoom {
  name: String;
  mentorId: String;
  timeStamp: Date;
}
