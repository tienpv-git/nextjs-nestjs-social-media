import { Module } from '@nestjs/common';
import { ClassRoomController } from './classRoom.controller';
import { ClassRoomService } from './classRoom.service';

@Module({
  controllers: [ClassRoomController],
  providers: [ClassRoomService],
})
export class ClassRoomModule {}
