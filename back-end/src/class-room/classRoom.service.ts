import { Injectable } from '@nestjs/common';
const {
  GoogleGenerativeAI,
  HarmCategory,
  HarmBlockThreshold,
} = require('@google/generative-ai');
const MODEL_NAME = 'gemini-1.0-pro-001';
const API_KEY = 'AIzaSyADjGK7cj0Dpi624ReL8KkwKrAp2UsxQwE';

@Injectable()
export class ClassRoomService {
  async getRandomWrongAnswers(
    questionContext: String,
    trueAnswerContext: String,
    trueAnswerNumber: number,
  ) {
    const genAI = new GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({ model: MODEL_NAME });

    const generationConfig = {
      temperature: 0.9,
      topK: 1,
      topP: 1,
      maxOutputTokens: 2048,
    };

    const safetySettings = [
      {
        category: HarmCategory.HARM_CATEGORY_HARASSMENT,
        threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
      },
      {
        category: HarmCategory.HARM_CATEGORY_HATE_SPEECH,
        threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
      },
      {
        category: HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT,
        threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
      },
      {
        category: HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT,
        threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
      },
    ];

    const chat = model.startChat({
      generationConfig,
      safetySettings,
      history: [],
    });

    const result = await chat.sendMessage(
      `here is the question: "${questionContext}" and the correct answer is "${trueAnswerContext}", create ${trueAnswerNumber - 1} wrong answers. Include only ${trueAnswerNumber} answer (includes correct answer given) without any other content.`,
    );
    let answers = result.response?.candidates[0]?.content?.parts[0]?.text;
    const specialCharRegex = /[!@#$%^&*()_+\-=\[\]{};':"\\|<>\/?~.,0-9]/g;
    answers = answers
      .toLowerCase()
      .replaceAll(specialCharRegex, '')
      .trim()
      .split(/\n\s+/);
    return this.shuffle(answers);
  }

  shuffle(array) {
    let currentIndex = array.length,
      randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex],
        array[currentIndex],
      ];
    }
    return array;
  }
}
