import {
  Controller,
  Get,
  Req,
  Res,
  HttpCode,
  Header,
  Redirect,
  Param,
  Post,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ClassRoomService } from './classRoom.service';

@Controller('class-room')
export class ClassRoomController {
  constructor(private classRoomService: ClassRoomService) {}

  @Get()
  async main(@Req() req: Request, @Res() res: Response): Promise<void> {
    const questionContext =
      req.body.questionContext ||
      'The data lines provide a path for moving data among system modules and are collectively called the _________.';
    const trueAnswerContext = req.body.questionContext || 'data bus';
    const trueAnswerNumber = req.body.questionContext || 4;
    const answers = await this.classRoomService.getRandomWrongAnswers(
      questionContext,
      trueAnswerContext,
      trueAnswerNumber,
    );
    res.json(answers);
  }
}
